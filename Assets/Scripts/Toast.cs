﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Toast : MonoBehaviour {

    private static GameObject _toast;
    private static Text _message;
    private float _timer=3f,_counter=0;

    private Toast()
    {
        
    }
    public static string Message { get; set; }

    public static void SetPosition(float x, float y)
    {
        _toast.transform.position = new Vector3(x,y,_toast.transform.position.z);
    }

    public static void Show()
    {
        GameObject canvas = GameObject.Find("Canvas");
        _toast = (GameObject)Instantiate(Resources.Load("Toast"));
        _toast.transform.SetParent(canvas.transform);
        _message = _toast.GetComponentInChildren<Text>();
        _message.text = Message;

    }

    // Update is called once per frame
    void Update ()
    {
        _counter += Time.deltaTime;

        if (_counter > _timer)
        {
            Destroy(_toast);    
        }
    }
}
