﻿using UnityEngine;
using UnityEngine.UI;

public class DialogBox : MonoBehaviour
{
    private static Text _message;
    private static Button _btnConfirm;
    private static GameObject _dialogBox;
    private DialogBox()
    {

    }


    public static string Message { get; set; }

    public static void SetPosition(float x, float y)
    {
        _dialogBox.transform.position = new Vector3(x, y, _dialogBox.transform.position.z);
    }

    public static void Show()
    {
        GameObject canvas = GameObject.Find("Canvas");
        _dialogBox = (GameObject)Instantiate(Resources.Load("DialogBox"));
        _dialogBox.transform.SetParent(canvas.transform);
        _message = _dialogBox.GetComponentInChildren<Text>();
        _message.text = Message;

        _btnConfirm = _dialogBox.GetComponentInChildren<Button>();
        _btnConfirm.onClick.AddListener(() => { GameObject.Destroy(_dialogBox); });
    }
}
