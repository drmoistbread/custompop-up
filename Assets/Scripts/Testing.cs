﻿using UnityEngine;

public class Testing : MonoBehaviour
{

    public void ToastButton()
    {
        Toast.Message = "Toast!!!";
        Toast.Show();
        Toast.SetPosition(300, 500);
    }

    public void DialogBoxButton()
    {
        DialogBox.Message = "DialogBox!!!";
        DialogBox.Show();
        DialogBox.SetPosition(300, 500);
    }
    public void FileExplorerButton()
    {
        CustomFileExplorerManager.GetInstance().ShowOpenFileWindow(s => Debug.Log("File Explorer " + s));
        CustomFileExplorerManager.GetInstance().SetPosition(400, 500);
    }
    public void InputBoxButton()
    {
        InputBox.Message = "InputBox!!!";
        InputBox.ConfirmButtonAction = s => Debug.Log("Input Box " + s);
        InputBox.Show();
        InputBox.SetPosition(300, 500);
    }
    public void ConfirmBoxButton()
    {
        ConfirmBox.Message = "ConfirmBox!!!";
        ConfirmBox.ConfirmButtonAction = () => Debug.Log("Confirm Box clicked");
        ConfirmBox.Show();
        ConfirmBox.SetPosition(300, 500);
    }

}
