﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class InputBox : MonoBehaviour
{

    private static Text _message;
    private static InputField _inputValue;

    private static Button _btnConfirm;

    private static Button _btnCancel;

    private static GameObject _inputBox;



    public static string Message { get; set; }
    public static Action<string> ConfirmButtonAction { set; get; }
    private InputBox()
    {

    }

    public static void SetPosition(float x, float y)
    {
        _inputBox.transform.position = new Vector3(x, y, _inputBox.transform.position.z);
    }

    public static void Show()
    {
        GameObject canvas = GameObject.Find("Canvas");
        _inputBox = (GameObject)Instantiate(Resources.Load("InputBox"));
        _inputBox.transform.SetParent(canvas.transform);

        _message = _inputBox.GetComponentInChildren<Text>();
        _message.text = Message;

        _inputValue = _inputBox.GetComponentInChildren<InputField>();

        _btnCancel = _inputBox.GetComponentInChildren<Button>();
        _btnCancel.onClick.AddListener(() => { GameObject.Destroy(_inputBox); });

        _btnConfirm = _inputBox.GetComponentsInChildren<Button>()[1];
        _btnConfirm.onClick.AddListener(() => { ConfirmButtonAction(_inputValue.text); });
        _btnConfirm.onClick.AddListener(() => { GameObject.Destroy(_inputBox); });

    }
}
