﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CustomFileExplorer : MonoBehaviour
{

    private InputField _fileName;
    private Text _title;
    private Text _confirmButtonText;
    private GameObject _fileExplorerPrefab;

    public void Awake()
    {
        _fileExplorerPrefab = (GameObject)Instantiate(Resources.Load("FileExplorer"));
        _fileExplorerPrefab.name = "FileExplorer";
        _fileName = GameObject.Find("FileNameInput").GetComponent<InputField>();
        _title = GameObject.Find("Title").GetComponent<Text>();
        _confirmButtonText = GameObject.Find("ConfirmButton").GetComponentInChildren<Text>();

        //put the prefab inside the canvas
        Canvas canvas = (Canvas)GameObject.Find("Canvas").GetComponent<Canvas>();
        _fileExplorerPrefab.transform.SetParent(canvas.transform, false);
    }

    public void ShowOpenWindow(Action<string> clickAction)
    {
        _fileName.enabled = false;
        _title.text = "OPEN FILE";
        _confirmButtonText.text = "OPEN";
        AddConfirmButtonListener(clickAction,/*isSaveAction=*/false);
    }

    public void ShowSaveWindow(Action<string> clickAction)
    {
        _fileName.enabled = true;
        _title.text = "SAVE FILE";
        _confirmButtonText.text = "SAVE";
        AddConfirmButtonListener(clickAction,/*isSaveAction=*/true);

    }
    private void AddConfirmButtonListener(Action<string> clickAction, bool isSaveAction)
    {
        Button confirmButton = GameObject.Find("ConfirmButton").GetComponent<Button>();
        confirmButton.onClick.AddListener(() =>
        {
            //I tried to put this inside a method, but does not work
            DirectoriesList directoriesList = GameObject.Find("DirectoriesList").GetComponent<DirectoriesList>();
            if (isSaveAction)
                clickAction(directoriesList.FolderToSave());
            else
                clickAction(directoriesList.FileToOpen());

            Destroy(GameObject.Find("FileExplorer"));
        });
    }

    public void SetPosition(float x, float y)
    {
        _fileExplorerPrefab.transform.position = new Vector3(x, y, _fileExplorerPrefab.transform.position.z);
    }
}



