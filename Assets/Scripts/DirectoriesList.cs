﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DirectoriesList : MonoBehaviour
{

    public GameObject ItemPrefab, DirectoriesUi;
    private const float Height = 50;
    private readonly List<string> _directories = new List<string>();
    private readonly List<string> _files = new List<string>();
    private string _currentDirectory;
    private Vector3 _contSize;
    public Button BackBtn;
    public Text CurrentPath;
    private RectTransform _contRectTransf;
    private InputField _FileToSaveName;

    private string _currentFile;

   

    private void Start()
    {

        _contRectTransf = gameObject.GetComponent<RectTransform>();
        _contSize = _contRectTransf.sizeDelta;
        _FileToSaveName = GameObject.Find("FileNameInput").GetComponent<InputField>();
        ResetToCurrentDirectory();
    }

    private void Update()
    {
        BackBtn.enabled = _currentDirectory != "C:";
        CurrentPath.text = _currentDirectory;
    }

    public void ResetToCurrentDirectory()
    {
        _currentDirectory = Directory.GetCurrentDirectory();
        _FileToSaveName.text = "";
        ListDirectories(_currentDirectory);

    }


    /// <summary>
    /// Checks for existing directories and files inside the _currentDirectory
    /// </summary>
    /// <param name="dirPath">Complete path to directory</param>
    public void ListDirectories(string dirPath)
    {
        _currentDirectory = dirPath;
        _directories.Clear();
        _files.Clear();

        var items = GameObject.FindGameObjectsWithTag("Item");
        foreach (var i in items) Destroy(i);


        if (Directory.Exists(_currentDirectory))
        {

            var di = new DirectoryInfo(_currentDirectory);

            var folders = di.GetDirectories();

            foreach (var folder in folders)
            {
                _directories.Add(folder.Name);
            }

            var diFiles = di.GetFiles();
            foreach (var file in diFiles)
            {
                if (file.Extension != ".lnk" && file.Extension != ".ini")
                    _files.Add(file.Name);
            }

            ListItems();
        }
    }

    /// <summary>
    /// Instantiate items
    /// </summary>
    private void ListItems()
    {
        foreach (var t in _directories)
        {
            var newItem = Instantiate(ItemPrefab);
            newItem.name = t;

            newItem.transform.SetParent(gameObject.transform);

            var btn = newItem.GetComponentInChildren<Button>();
            var btnText = btn.GetComponentInChildren<Text>();

            btn.onClick.AddListener(() =>
            {
                _currentDirectory = _currentDirectory + "\\" + btn.name;
                ListDirectories(_currentDirectory);
            });

            btnText.text = t;
        }

        foreach (var f in _files)
        {
            var newItem = Instantiate(ItemPrefab);
            newItem.name = gameObject.name + f;

            newItem.transform.SetParent(gameObject.transform);

            var btn = newItem.GetComponentInChildren<Button>();
            var btnText = btn.GetComponentInChildren<Text>();
            btn.GetComponent<Image>().color = new Color(1, 1, 1, 0.25f);
            var fileName = f;

            btn.onClick.AddListener(() =>
            {
                _currentFile = _currentDirectory + "\\" + fileName;
                _FileToSaveName.text = fileName;
            });

            btnText.text = f;
        }

        var scrollHeight = Height * (_directories.Count + _files.Count);

        _contRectTransf.sizeDelta = Vector2.zero;
        _contRectTransf.sizeDelta = new Vector2(_contSize.x, scrollHeight);
    }

    /// <summary>
    /// Lists the content of the previous selected directory
    /// </summary>
    public void BackToLastDirectory()
    {
        var words = _currentDirectory.Split(Path.DirectorySeparatorChar);
        var newPath = words[0];

        for (var i = 1; i < words.Length - 1; i++)
        {
            newPath = newPath + Path.DirectorySeparatorChar + words[i];
        }

        if (words.Length == 2)
            newPath = newPath + Path.DirectorySeparatorChar;

        _currentDirectory = newPath;
        ListDirectories(_currentDirectory);
    }

    public string FileToOpen()
    {
        return _currentFile;
    }

    public string FolderToSave()
    {
        return _currentDirectory + "\\" + _FileToSaveName.text;

    }
}
