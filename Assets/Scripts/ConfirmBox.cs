﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmBox : MonoBehaviour
{
    private static Text _message;
    private static GameObject _confirmBox;

    private static Button _btnConfirm;

    private static Button _btnCancel;

    public static string Message { get; set; }
    public static Action ConfirmButtonAction { set; get; }
    private ConfirmBox()
    {

    }

    public static void Show()
    {
        GameObject canvas = GameObject.Find("Canvas");
        _confirmBox = (GameObject)Instantiate(Resources.Load("ConfirmBox"));
        _confirmBox.transform.SetParent(canvas.transform);

        _message = _confirmBox.GetComponentInChildren<Text>();
        _message.text = Message;

        _btnCancel = _confirmBox.GetComponentInChildren<Button>();
        _btnCancel.onClick.AddListener(() => { GameObject.Destroy(_confirmBox); });

        _btnConfirm = _confirmBox.GetComponentsInChildren<Button>()[1];
        _btnConfirm.onClick.AddListener(() => { ConfirmButtonAction(); });
        _btnConfirm.onClick.AddListener(() => { GameObject.Destroy(_confirmBox); });

    }

    public static void SetPosition(float x, float y)
    {
        _confirmBox.transform.position = new Vector3(x, y, _confirmBox.transform.position.z);
    }
}
