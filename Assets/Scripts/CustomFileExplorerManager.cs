﻿using System;
using UnityEngine;

public class CustomFileExplorerManager
{

    private static CustomFileExplorerManager _manager;
    private static CustomFileExplorer _fileExplorer;
    private bool _isActive;

    private CustomFileExplorerManager()
    {
        _isActive = false;
    }

    public void ShowOpenFileWindow(Action<string> clickAction)
    {
        if (isFileWindowVisible()) return;

        _fileExplorer.ShowOpenWindow(clickAction);
    }

    public void ShowSaveFileWindow(Action<string> clickAction)
    {
        if (isFileWindowVisible()) return;

        _fileExplorer.ShowSaveWindow(clickAction);
    }

    private bool isFileWindowVisible()
    {
        if (_isActive)
        {
            DestroyFileExplorer();
            _isActive = false;
            return true;
        }
        CreateFileExplorerGameObject();
        _isActive = true;
        return false;
    }

    private void CreateFileExplorerGameObject()
    {
        GameObject fileExplorer = new GameObject();
        fileExplorer.AddComponent(typeof(CustomFileExplorer));
        _fileExplorer = fileExplorer.GetComponent<CustomFileExplorer>();

        //the fileExplorer GO is not needed anymore, cause it is just an empty GO.
        //the FileExplorer prefab was moved inside Canvas in CustomFileExplorer.class
        GameObject.Destroy(fileExplorer);
    }
    private static void DestroyFileExplorer()
    {
        GameObject.Destroy(GameObject.Find("FileExplorer"));
    }

    public void SetPosition(float x, float y)
    {
        _fileExplorer.SetPosition(x, y);
    }

    #region Singleton
    public static CustomFileExplorerManager GetInstance()
    {
        if (_manager == null)
            _manager = new CustomFileExplorerManager();
        return _manager;
    }

    #endregion
}
